# Shim Trays

Trays to hold magnets to increase the homogeneity of a [30cm-halbach-magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet/).

This design essentially uses 3D printed parts, brass rods and spacer sleeves – and magnets of course.

## Specifications

- compatible with [30cm-halbach-magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet/) `v2.0.*`, `v2.1.*`
- design as is supports shim magnets with an edge length up to 9mm

## Authors and acknowledgment

Contributors (alphabetical order): Martin Häuer, Christian Höhn, Tom O'Reilly

## License and Liability

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and [DISCLAIMER](DISCLAIMER.pdf).

