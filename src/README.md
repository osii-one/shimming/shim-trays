# Source README

**PLEASE NOTE** that the FreeCAD design makes extensive use of its Spreadsheet feature – please keep those intact when making adjustments.

**Orientation:** FreeCAD TOP view = Front view on the real magnet, B0 pointing upwards in FreeCAD

## Shim Magnet Variations

The design is set with parameters 6mm cube magnets:

- `shimming magnet width` = 6mm (see FreeCAD spreadsheets)
- `cornerRadius` = 0.8mm (see FreeCAD macro)
- ergo: `magnet insert radius` = 5mm (see FreeCAD spreadsheets)

The maximum magnet size, keeping the rest of the design unchanged, should be 9mm:

- `shimming magnet width` = 9mm
- `cornerRadius` = 1.0mm
- `magnet insert radius` = 8mm

For larger magnets, the number of magnet inserts per shim insert would need to be adjusted; the CAD files are currently not parameterized in this regard.

## File Formats

The original design has been created with Solidworks.
While everything around the shim-inserts has been ported to FreeCAD, the shim tray has not yet.
Please refer to the respective [release notes](https://gitlab.com/osii/shimming/shim-trays/-/releases/) for the respective export files (STL).

## Making the Magnet Pockets

The printable magnet insert files are created from the template file `magnet-insert.FCStd`:

> **NOTE:** the orientation in this file: FreeCAD-TOP-View = looking from the back of the magnet towards the front (B0 in pos. y /= points towards the top)

1. copy the FreeCAD macro (`shimming-magnets.FCMacro`) into your local FreeCAD macro directory
	> e.g. `/home/USER/.var/app/org.freecadweb.FreeCAD/data/FreeCAD/Macro/` if you're on Linux and have FreeCAD installed over flathub
2. enter the file path to the corresponding input file in line 19 of the macro
	> e.g. `/home/USER/git/shim-trays/src/magnet-rotations-sample/sample1`
3. execute macro in FreeCAD (top menu bar: Macro → Macros… → select rotatingMagnets.FCMacro → Execute
4. at the macro prompt: enter 4 to create squares and circles in separate folders
5. double click on the shim body in which you want to cut the pockets (e.g. `shim1`), mark all corresponding squares and execute `Create sub-object(s) shape binder` (option under `Part Design` (menu bar)); repeat for circles
6. use the newly created objects to create the pockets
    > note that squares and circles have to be cut seperately (FreeCAD v0.21.2 doesn't like when sketches are overlapping)
7. repeat the previous 2 steps for the other shim bodies

