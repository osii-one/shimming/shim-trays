# Assembly Guide

## Preparing the Parts

The tray design consists of universal parts that can be prepared before shimming and case-specific parts that incorporate the results from the shimming script.

Universal parts (to make/prepare) are:

```
front
back1…12
shim-insert
rod4
```

Cut xxx mm M4 thread on each side of `rod4`.
The other parts just have to be printed

Case-specific parts are:

```
magnet-insert
```

Please follow the steps in the [Source README](/src/README.md) to create the magnet inserts.

Once all parts are printed, mark north for all magnets.

## Assembling the Shim-Inserts

- press magnets into the magnet insert; apply superglue if needed
- press the magnet inserts into the shim insert
    > the banana-shaped base should allow to press them in all at once

## Assembling the Trays

As for tray1 for instance:

```
1× front
1× back1
x× assembled shim-insert (quantity according to your simulation results)
2× rod4
2× M4sleeve
4× M4nut
2× M4washer
spacers
```

Mount them as shown in the picture below:

xxx

Note that every `back` has a specific mark that corresponds to its final position at the magnet.
As for `back4` for instance: The pointer always points in B0 direction of the field; the circles indicate the tray number and the mounting position of the tray.

![tray and B0 mark of back4](/res/back4-screenshot.png)\
_tray and B0 mark of `back4`_

